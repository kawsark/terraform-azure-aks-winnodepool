// Outputs
output "kube_config" {
  value     = module.aks_winnodepool_module.kube_config
  sensitive = true
}

