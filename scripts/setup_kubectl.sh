#!/bin/bash

command=$(echo $0 | grep source)
[[ -z ${command} ]] && echo "hint: use source ./setup_kubectl.sh"  

# Source this script to download state file URL so that we can work with kubectl
[[ -z $url ]] && echo "Please export url environment variable to the workspace state file URL" && exit 1

echo "INFO: Downloading state file"
wget -O terraform.tfstate ${url}

echo "INFO: Saving kube config"
rm -f aks.yaml
terraform output -raw kube_config > aks.yaml

echo "INFO: Updating KUBECONFIG environment variable"
export KUBECONFIG=$(pwd)/aks.yaml:${KUBECONFIG}

echo "INFO: issuing kubectl get pods"
kubectl get pods
